<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
			new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
			new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
			new ES\Bundle\BaseBundle\ESBaseBundle(),
			new FOS\UserBundle\FOSUserBundle(),
			new ES\Bundle\UserBundle\ESUserBundle(),
			new Braincrafted\Bundle\BootstrapBundle\BraincraftedBootstrapBundle(),
			new ES\Bundle\FileUploadBundle\ESFileUploadBundle(),
			new ES\Bundle\FixturesBundle\ESFixturesBundle(),
			new Sonata\CoreBundle\SonataCoreBundle(),
			new Sonata\BlockBundle\SonataBlockBundle(),
			new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
			new Sonata\AdminBundle\SonataAdminBundle(),
			new Knp\Bundle\MenuBundle\KnpMenuBundle(),
			new Liip\ImagineBundle\LiipImagineBundle(),
            new AppBundle\AppBundle(),
            new Ornicar\ApcBundle\OrnicarApcBundle(),
            new ES\Bundle\SocialBundle\ESSocialBundle(),
            new ES\Bundle\NotificationBundle\ESNotificationBundle(),
            new Sonata\NotificationBundle\SonataNotificationBundle(),
            new Knp\Bundle\TimeBundle\KnpTimeBundle(),
            new Lexik\Bundle\WorkflowBundle\LexikWorkflowBundle(),
            new Cnerta\BreadcrumbBundle\CnertaBreadcrumbBundle(),
            new ES\Bundle\AdminBundle\ESAdminBundle(),
            new AerialShip\SamlSPBundle\AerialShipSamlSPBundle(),
            new WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle(),
            new FOS\ElasticaBundle\FOSElasticaBundle(),
            new ES\Bundle\MessageBundle\ESMessageBundle(),
            new FOS\MessageBundle\FOSMessageBundle(),
            new ES\Bundle\CmsBundle\ESCmsBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'])) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
