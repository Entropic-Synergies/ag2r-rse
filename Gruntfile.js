module.exports = function (grunt) {
	'use strict';

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		rootDir: '<%= pkg.mainPath %>/Resources',
		httpPath: '/',
		assetsPath: '<%= httpPath %>bundles/<%= pkg.bundleShortName %>/',
		compass: {
			dist: {
				options: {
					raw: 'preferred_syntax = :scss\n',
					noLineComments: true,
					outputStyle: 'compressed',
					basePath: '<%= rootDir %>',
					cssDir: 'public/css',
					sassDir: 'sass',
					imagesDir: 'public/images',
					httpPath: '<%= httpPath %>',
					httpImagesPath: '<%= assetsPath %>images/',
					httpGeneratedImagesPath: '<%= assetsPath %>images/',
					relativeAssets: false,
					force: false
				}
			}
		},
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "<%= rootDir %>/public/css/style.css": "<%= rootDir %>/less/main.less",
                    "<%= rootDir %>/public/css/cms.css": "<%= rootDir %>/less/cms.less"
                }
            }
        },

		cssmin: {
			minify: {
				keepSpecialComments: 0,
				expand: true,
				cwd: '<%= rootDir %>/public/css/',
				src: ['**/**/*.css', '!*.min.css'],
				dest: '<%= rootDir %>/public/css/',
				ext: '.min.css'
			}
		},

		watch: {
			grunt: {
				files: ['Gruntfile.js']
			},
			css: {
				files: [
					'<%= rootDir %>/sass/**/**/*.scss',
					'<%= rootDir %>/less/**/**/*.less',
					'vendor/es/social-bundle/ES/Bundle/SocialBundle/Resources/sass/*.scss'
				],
				tasks: ['default']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['compass', 'less', 'cssmin']);
	grunt.registerTask('dev', ['default', 'watch']);

};
