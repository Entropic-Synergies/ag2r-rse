echo 'Fetch git...'
git pull
echo 'Composer install...'
composer install
echo 'Clear cache...'
rm -rf app/cache/*
echo 'Migrate DB...'
app/console doctrine:migration:migrate --no-interaction
echo 'Reset fixtures...'
app/console es:fixtures:load
echo 'Dump assetic...'
app/console assetic:dump