<?php

namespace AppBundle\Model;

use AppBundle\Entity\Idea;
use AppBundle\Entity\User;
use AppBundle\Workflow\IdeaWorkflow;
use Doctrine\ORM\EntityManager;
use ES\Bundle\NotificationBundle\Model\NotificationManagerInterface;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessHandlerInterface;

class IdeaManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var NotificationManagerInterface
     */
    private $notificationManager;

    /**
     * @var ProcessHandlerInterface
     */
    private $processHandler;

    function __construct(EntityManager $em, NotificationManagerInterface $notificationManager, ProcessHandlerInterface $processHandler)
    {
        $this->em                  = $em;
        $this->notificationManager = $notificationManager;
        $this->processHandler      = $processHandler;
    }

    public function reachNextState(Idea $idea, $state, User $user)
    {
        $ideaWorkflow = new IdeaWorkflow($idea);

        $modelState = $this->processHandler->reachNextState($ideaWorkflow, $state);

        if (!$modelState->getSuccessful()) {
            if (!$modelState->getSuccessful()) {
                throw new \LogicException(implode(', ', $modelState->getErrors()));
            }
        }

        $this->em->persist($idea);
        $this->em->flush();

        $notification = $this->notificationManager->createNotification(
            $user->getSubjectReference(),
            'idea_status_change.' . $idea->getStatus(),
            $idea->getSubjectReference()
        );
        $notification->setMetadata([
            'admin_message' => $idea->getAdminMessage(),
        ]);

        $this->notificationManager->notifyUsers($notification, [$idea->getOwner()]);
    }
}