<?php


namespace AppBundle\Model;

use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

class IdeaShare
{
    /**
     * @var User[]|ArrayCollection
     */
    protected $users;

    /**
     * @var string
     */
    protected $message;

    function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return \AppBundle\Entity\User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param \AppBundle\Entity\User[]|ArrayCollection $users
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }
}