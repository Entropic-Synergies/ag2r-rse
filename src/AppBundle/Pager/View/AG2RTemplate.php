<?php


namespace AppBundle\Pager\View;

use Pagerfanta\View\Template\TwitterBootstrapTemplate;

class AG2RTemplate extends TwitterBootstrapTemplate
{
    static protected $defaultOptions = [
        'prev_message'        => '&lt;',
        'next_message'        => '&gt;',
        'dots_message'        => '&hellip;',
        'active_suffix'       => '',
        'css_container_class' => '',
        'css_prev_class'      => 'prev',
        'css_next_class'      => 'next',
        'css_disabled_class'  => 'disabled',
        'css_dots_class'      => 'disabled',
        'css_active_class'    => 'active',
    ];
} 