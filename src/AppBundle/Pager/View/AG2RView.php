<?php
namespace AppBundle\Pager\View;

use Pagerfanta\View\TwitterBootstrapView;

class AG2RView extends TwitterBootstrapView
{
    protected function createDefaultTemplate()
    {
        return new AG2RTemplate();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ag2r';
    }
}