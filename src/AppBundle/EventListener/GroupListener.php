<?php


namespace AppBundle\EventListener;

use ES\Bundle\UserBundle\Event\GroupEvent;
use ES\Bundle\UserBundle\EventListener\GroupEvents;
use Sonata\NotificationBundle\Backend\BackendInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GroupListener implements EventSubscriberInterface
{
    const QUEUE_NAME = 'group_feed';

    /**
     * @var BackendInterface
     */
    private $notificationProducer;

    function __construct(BackendInterface $notificationProducer)
    {
        $this->notificationProducer = $notificationProducer;
    }

    public static function getSubscribedEvents()
    {
        return [
            GroupEvents::GROUP_PRIVACY_CHANGED => 'onGroupPrivacyChanged',
        ];
    }

    public function onGroupPrivacyChanged(GroupEvent $event)
    {
        if (null !== $privacyWay = $event->getPrivacyWay()) {
            $this->notificationProducer->createAndPublish(self::QUEUE_NAME, [
                'group' => $event->getGroup()->getId(),
                'way'   => $privacyWay,
            ]);
        }
    }
} 