<?php


namespace AppBundle\EventListener;

use ES\Bundle\NotificationBundle\Utils\MaskBuilder;
use ES\Bundle\SocialBundle\Event\SocialInteractionEvent;
use ES\Bundle\SocialBundle\EventListener\InteractionNotifyerListener;
use ES\Bundle\SocialBundle\Model\VoteInterface;

class VoteNotifyerListener extends InteractionNotifyerListener
{
    static protected $type = 'vote';

    protected function getSubscriptionMask()
    {
        return MaskBuilder::MASK_LIKE;
    }

    public function handleEvent(SocialInteractionEvent $event)
    {
        /** @var VoteInterface $interaction */
        $interaction = $event->getInteraction();

        if ($interaction->getWay() === 1) {
            $object           = $interaction->getObject();
            $subjectReference = $interaction->getSubjectReference();

            $notification = $this->notificationManager->createNotification($interaction->getUser()->getSubjectReference(), 'vote.up', $object, $subjectReference);
            $this->notificationManager->notify($notification, 'like');
            $this->notificationManager->subscribe($interaction->getUser(), $object, $this->getSubscriptionMask());
        }
    }
} 