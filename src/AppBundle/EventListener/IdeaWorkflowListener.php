<?php


namespace AppBundle\EventListener;

use AppBundle\Entity\Idea;
use Doctrine\ORM\EntityManager;
use ES\Bundle\BaseBundle\Security\Acl\AclManager;
use ES\Bundle\NotificationBundle\Model\NotificationManagerInterface;
use ES\Bundle\SocialBundle\Manager\FeedManager;
use Lexik\Bundle\WorkflowBundle\Event\StepEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class IdeaWorkflowListener implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AclManager
     */
    protected $aclManager;

    /**
     * @var FeedManager
     */
    protected $feedManager;

    /**
     * @var NotificationManagerInterface
     */
    protected $notificationManager;

    function __construct(EntityManager $em, AclManager $aclManager, FeedManager $feedManager, NotificationManagerInterface $notificationManager)
    {
        $this->em                  = $em;
        $this->aclManager          = $aclManager;
        $this->feedManager         = $feedManager;
        $this->notificationManager = $notificationManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            'idea_publication.study_pending.reached' => [
                'handleStudyPendingReached',
            ],
        ];
    }

    public function handleStudyPendingReached(StepEvent $event)
    {
        /** @var Idea $idea */
        $idea = $event->getModel()->getWorkflowObject();

        if (null !== ($category = $idea->getCategory()) && null !== $category->getOperator()) {
            $idea->addOperator($category->getOperator());
        }
        foreach ($idea->getOperators() as $operator) {
            $this->aclManager->grantUserOnObject($operator, $idea, MaskBuilder::MASK_OPERATOR);
        }
        $notification = $this->notificationManager->createNotification($idea->getOwner()->getSubjectReference(), 'assigned_operator', $idea->getSubjectReference(), $idea->getSubjectReference());
        $this->notificationManager->notifyUsers($notification, $idea->getOperators()->getValues());

        $feed = $this->feedManager->createFeed($idea->getOwner(), 'create', $idea, $idea, $idea->getGroup());
        $this->feedManager->saveFeed($feed);

        if (null !== $group = $idea->getGroup()) {
            $group->updateIdeasCount(1);
            $this->em->persist($group);

            $notification = $this->notificationManager->createNotification(
                $idea->getOwner()->getSubjectReference(),
                'add_group_idea',
                $idea->getSubjectReference(),
                $group->getSubjectReference()
            );
            $this->notificationManager->notify($notification, \ES\Bundle\NotificationBundle\Utils\MaskBuilder::MASK_INFO, $idea->getGroup()->getSubjectReference());

            $this->aclManager->setObjectAncestor($idea, $group);
        } else {
            $this->aclManager->grantRoleOnObject('ROLE_USER', $idea, MaskBuilder::MASK_VIEW);
        }
    }
}