<?php

namespace AppBundle\Doctrine;

use AppBundle\Entity\Idea;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class IdeaListener implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::preRemove,
        ];
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Idea && null !== $group = $entity->getGroup()) {
            $group->updateIdeasCount(-1);
            $em = $args->getEntityManager();
            $em->persist($group);
        }
    }
}
