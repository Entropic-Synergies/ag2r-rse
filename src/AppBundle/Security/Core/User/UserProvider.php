<?php

namespace AppBundle\Security\Core\User;

use AerialShip\SamlSPBundle\Bridge\SamlSpInfo;
use AerialShip\SamlSPBundle\Security\Core\User\UserManagerInterface;
use AppBundle\Entity\User;
use FOS\UserBundle\Model\UserManager;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

class UserProvider extends ContainerAware implements UserManagerInterface
{
    /**
     * @var UserManager
     */
    private $userProvider;

    function __construct(UserManager $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    /**
     * @param SamlSpInfo $samlInfo
     * @return UserInterface
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserBySamlInfo(SamlSpInfo $samlInfo)
    {
        return $this->loadUserByUsername($samlInfo->getNameID()->getValue());
    }

    /**
     * @param SamlSpInfo $samlInfo
     * @throws \Symfony\Component\Security\Core\Exception\UsernameNotFoundException if the user could not created
     * @return \Symfony\Component\Security\Core\User\UserInterface
     */
    public function createUserFromSamlInfo(SamlSpInfo $samlInfo)
    {
        /** @var User $user */
        $user = $this->userProvider->createUser();

        $attributes = $samlInfo->getAttributes();
        $user->setEmail($attributes['emailAddress']->getFirstValue());
        $user->setLastname($attributes['nom']->getFirstValue());
        $user->setFirstname($attributes['prenom']->getFirstValue());

        list($login, $domain) = explode('@', $attributes['login']->getFirstValue(), 2);

        $domain = strtolower($domain);
        switch ($domain) {
            case 'ag2rprd.gie.net':
                $loginName = 'ag2rprd';
                break;
            case 'lamondiale.com':
                $loginName = 'mons';
                break;
            default:
                $loginName = 'comptes';
                break;
        }
        $loginName .= '\\' . $login;
        $user->setLoginName($loginName);

        $user->setPlainPassword(substr(md5(uniqid(time())), 0, 8));

        $this->container->get('es_base.mailer')->sendToUser('AppBundle:Mail:registration.html.twig', [], $user);

        return $user;
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @see UsernameNotFoundException
     *
     * @throws UsernameNotFoundException if the user is not found
     *
     */
    public function loadUserByUsername($username)
    {
        return $this->userProvider->loadUserByUsername($username);
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->userProvider->refreshUser($user);
    }

    /**
     * Whether this provider supports the given user class
     *
     * @param string $class
     *
     * @return Boolean
     */
    public function supportsClass($class)
    {
        return $this->userProvider->supportsClass($class);
    }
}
