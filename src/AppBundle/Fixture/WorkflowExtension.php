<?php

namespace AppBundle\Fixture;

use AppBundle\Entity\Idea;
use AppBundle\Entity\User;
use AppBundle\Workflow\IdeaWorkflow;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContextInterface;

class WorkflowExtension
{
    /**
     * @var ProcessHandlerInterface
     */
    private $processHandler;

    private $em;

    function __construct(ProcessHandlerInterface $processHandler, SecurityContextInterface $securityContext, EntityManager $em)
    {
        $this->processHandler = $processHandler;
        $user                 = new User();
        $user->setEnabled(true);
        $token = new UsernamePasswordToken($user, '', 'main', ['ROLE_USER', 'ROLE_ADMIN']);
        $securityContext->setToken($token);
        $this->em = $em;
    }

    public function initState(Idea $idea)
    {
        $ideaWorkflow = new IdeaWorkflow($idea);
        $this->processHandler->start($ideaWorkflow);
    }

    public function processStates(Idea $idea, array $steps = [])
    {
        $ideaWorkflow = new IdeaWorkflow($idea);

        foreach ($steps as $state) {
            $modelState = $this->processHandler->reachNextState($ideaWorkflow, $state);
            if (!$modelState->getSuccessful()) {
                if (!$modelState->getSuccessful()) {
                    throw new \LogicException(implode(', ', $modelState->getErrors()));
                }
            }
        }
        $this->em->persist($idea);
    }
}