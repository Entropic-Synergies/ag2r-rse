Feature: SAML2
  As an user not logged
  I want to login via SAML

  Scenario: First connection
    Given I am on "/"
    When I follow "Connexion automatique"

    Then I should be on "http://localhost:8082/module.php/core/loginuserpass.php"
    When I fill in the following:
      | username | arthur |
      | password | xxx    |
    And I press "Login"

    Then I should be on "http://localhost:8082/module.php/core/loginuserpass.php"
    When I press "Submit"

    Then I should be on "/"
    And I should see "Test User"

    When I follow "Se déconnecter"
    Then I should be on "/login"
    And I should not see "Test User"
