Feature: Documents
  Test access on documents

  #@javascript
  Scenario: Documents on an open group
    Given I am on "/login"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"

    When I am on "/groups"
    And I follow "Open group"
    Then I should be on "/groups/1"
    And I click on "#share-document"
    And I fill in the following:
      | document_form[name] | My document |
    And I attach the file "src/AppBundle/Resources/fixtures/images/user-1.jpg" to "document_form[file][file]"
    And I press "document_form[submit]"

    Then I should be on "/groups/1/documents"
    And I should see "My document"

    When I follow "Télécharger"
    Then the response status code should be 200

    Given I am on "/logout"
    When I fill in "_username" with "es-projects+test-homer@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Given I am on "/groups/1/documents"
    And I should see "My document"
    When I follow "Télécharger"
    Then the response status code should be 200

  #@javascript
  Scenario: Documents on a closed group
    Given I am on "/login"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"

    When I am on "/groups"
    And I follow "Closed group"
    Then I should be on "/groups/2"
    And I click on "#share-document"
    And I fill in the following:
      | document_form[name] | My closed document |
    And I attach the file "src/AppBundle/Resources/fixtures/images/user-1.jpg" to "document_form[file][file]"
    And I press "document_form[submit]"

    Then I should be on "/groups/2/documents"
    And I should see "My closed document"

    When I follow "Télécharger"
    Then the response status code should be 200

    Given I am on "/logout"
    When I fill in "_username" with "es-projects+test-homer@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Given I am on "/groups/2/documents"
    And I should not see "My closed document"
    When I follow "Rejoindre le groupe"
    Then I should see "Votre demande de joindre le groupe Closed group a été envoyée"

    Given I am on "/logout"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Given I am on "/groups/2/users"
    When I follow "Accepter"
    Then I should see "Vous avez accepté la demande de Homer Simpson"

    Given I am on "/logout"
    When I fill in "_username" with "es-projects+test-homer@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Given I am on "/groups/2/documents"
    And I should see "My closed document"

    When I follow "Télécharger"
    Then the response status code should be 200
