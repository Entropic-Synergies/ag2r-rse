Feature: Suggest
  Suggest contacts on sidebar

  #@javascript
  Scenario: Friends suggest
    Given I am on "/login"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"

    When I am on the homepage
    Then I should see "Homer Simpson" in the ".members-recommandation" element
    Then I should see "Lisa Simpson" in the ".members-recommandation" element
    Then I should see "Marge Simpson" in the ".members-recommandation" element
    Then I should see "Bart Simpson" in the ".members-recommandation" element
    Then I should not see "Chuck Norris" in the ".members-recommandation" element

    When I click on ".members-recommandation .user:nth-child(3) a.pull-right.message"
    Then I should be on "/"
    Then I should see "Votre demande a été envoyée à Marge Simpson"
    Then I should see "Homer Simpson" in the ".members-recommandation" element
    Then I should see "Lisa Simpson" in the ".members-recommandation" element
    Then I should see "Bart Simpson" in the ".members-recommandation" element
    Then I should see "Chuck Norris" in the ".members-recommandation" element
    Then I should not see "Marge Simpson" in the ".members-recommandation" element
