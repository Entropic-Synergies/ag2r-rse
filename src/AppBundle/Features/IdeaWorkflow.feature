Feature: Idea workflow
  Test the full cycle of an idea

  Scenario: Create draft idea
    Given I am on "/login"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Test User" in the "#user-menu" element

    And I fill in the following:
      | idea_form[name]        | My test draft idea |
      | idea_form[description] | Lorem ipsum ...    |
    And I press "idea_form[save_draft]"
    Then I should be on "/ideas/new"
    And I should see "Ce champ est obligatoire."

    Then I select "Idées commerciales" from "idea_form[category]"
    And I press "idea_form[save_draft]"

    Then I should be on "/"
    And I should see "Votre idée a bien été enregistrée dans vos brouillons"
    When I follow "Catalogue des idées"
    Then I should be on "/ideas"
    And I should see "My test draft idea"
    When I am on "/ideas/3"
    And I should see "My test draft idea"

    # Check ROLE_USER don't see the draft
    When I am on "/logout"
    Then I should be on "/login"
    When I fill in "_username" with "es-projects+test-homer@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Homer Simpson" in the "#user-menu" element
    And I should not see "My test draft idea"
    When I am on "/ideas"
    Then I should not see "My test draft idea"
    When I am on "/ideas/3"
    And I should see "403 Forbidden"

  Scenario: Create idea
    Given I am on "/login"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Test User" in the "#user-menu" element

    And I fill in the following:
      | idea_form[name]        | My test idea    |
      | idea_form[description] | Lorem ipsum ... |
    Then I select "Idées commerciales" from "idea_form[category]"
    And I press "idea_form[submit]"

    Then I should be on "/"
    And I should see "Votre idée a bien été proposée"
    Then I should not see "My test idea"

    # Check ROLE_USER don't see the idea
    When I am on "/logout"
    Then I should be on "/login"
    When I fill in "_username" with "es-projects+test-homer@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Homer Simpson" in the "#user-menu" element
    And I should not see "My test idea"
    When I am on "/ideas"
    Then I should not see "My test idea"
    When I am on "/ideas/3"
    And I should see "403 Forbidden"

    When I am on "/logout"
    Then I should be on "/login"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Test User" in the "#user-menu" element
    When I am on "/admin"
    And I follow "Idea"
    And I follow "Valider"
    Then I should see "Changement d'état réussi"

    # Check ROLE_USER can see the idea
    When I am on "/logout"
    Then I should be on "/login"
    When I fill in "_username" with "es-projects+test-homer@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Homer Simpson" in the "#user-menu" element
    And I should see "My test idea"
    When I am on "/ideas"
    Then I should see "My test idea"
    When I am on "/ideas/3"
    And I should see "My test idea"

    When I am on "/logout"
    Then I should be on "/login"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Test User" in the "#user-menu" element
    When I am on "/admin"
    And I follow "Idea"
    And I follow "Passer à l'étude"
    Then I should see "Changement d'état réussi"

    # Check ROLE_USER still can't see the idea
    When I am on "/logout"
    Then I should be on "/login"
    When I fill in "_username" with "es-projects+test-homer@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Homer Simpson" in the "#user-menu" element
    Then I should see "Test User a créé l'idée My test idea"
    When I am on "/ideas"
    Then I should see "My test idea"
    When I am on "/ideas/3"
    Then I should see "My test idea"

  Scenario: Make group idea public
    Given I am on "/login"
    When I fill in "_username" with "es-projects+test@entropic-synergies.com"
    When I fill in "_password" with "xxx"
    And I press "_submit"
    Then I should be on "/"
    And I should see "Test User" in the "#user-menu" element

    Given I am on "/groups"
    When I follow "Open group"
    Then I should be on "/groups/1"
    When I follow "Idées"
    Then I should be on "/groups/1/ideas"
    And I should see:
      | 2 idées référencées |
      | idées (2)           |

    And I fill in the following:
      | idea_form[name]        | My group test idea |
      | idea_form[description] | Lorem ipsum ...    |
    Then I select "Catégorie de groupe #1" from "idea_form[category]"
    And I press "idea_form[submit]"

    Then I should be on "/groups/1/ideas"
    And I should see:
      | Votre idée a bien été proposée |
      | My group test idea             |
      | Catégorie de groupe #1         |
      | 3 idées référencées            |

    When I follow "My group test idea"
    Then I should be on "/groups/1/ideas/3"
    When I follow "Rendre cette idée publique"
    Then I should be on "/ideas/3/make-public"
    When I select "Idées commerciales" from "make_idea_public_form[category]"
    And I press "make_idea_public_form[submit]"
    Then I should see:
      | My group test idea                |
      | L'idée a bien été rendue publique |
      | Idées commerciales                |

    Given I am on "/groups/1/ideas"
    Then I should see:
      | 2 idées référencées |
      | idées (2)           |