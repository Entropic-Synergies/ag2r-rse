<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Idea;
use AppBundle\Workflow\IdeaWorkflow;
use Lexik\Bundle\WorkflowBundle\Flow\Process;
use Lexik\Bundle\WorkflowBundle\Flow\Step;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessAggregator;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessHandlerInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class IdeaAdmin extends Admin
{
    /**
     * @var ProcessHandlerInterface
     */
    protected $workflow;

    /**
     * @var ProcessAggregator
     */
    protected $processAggregator;

    /**
     * @param ProcessAggregator $processAggregator
     */
    public function setProcessAggregator(ProcessAggregator $processAggregator)
    {
        $this->processAggregator = $processAggregator;

        return $this;
    }

    /**
     * @param ProcessHandlerInterface $workflow
     */
    public function setWorkflow(ProcessHandlerInterface $workflow)
    {
        $this->workflow = $workflow;

        return $this;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->add('change_status', $this->getRouterIdParameter() . '/change_status');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name', null, [
                'label' => 'Titre',
            ])
            ->add('owner.displayName', null, [
                'label' => 'Auteur',
            ])
            ->add('category.name', null, [
                'label' => 'Thématique',
            ])
            ->add('status', null, [
                'label'    => 'Statut',
                'template' => 'AppBundle:Admin/CRUD:list_field_status.html.twig'
            ])
            ->add('_action', 'actions', [
                'actions' => [
                    'change_status' => [
                        'template' => 'AppBundle:Admin/CRUD:list__action_change_status.html.twig'
                    ]
                ]
            ]);
    }

    public function createQuery($context = 'list')
    {
        $proxyQuery = parent::createQuery($context);
        // Default Alias is "o"
        $proxyQuery->orderBy('o.createdAt', 'DESC');
        $proxyQuery->andWhere('o.status != :draft')
            ->setParameter('draft', Idea::STATUS_DRAFT);

        return $proxyQuery;
    }

    public function getAvailableNextSteps(Idea $idea)
    {
        $model      = new IdeaWorkflow($idea);
        $modelState = $this->workflow->getCurrentState($model);

        /** @var Process $process */
        $process    = $this->processAggregator->getProcess('idea_publication');
        /** @var Step $step */
        $step       = $process->getStep($modelState->getStepName());
        $nextStates = $step->getNextStates();

        return $nextStates;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('category')
            ->add('description');
    }
}
