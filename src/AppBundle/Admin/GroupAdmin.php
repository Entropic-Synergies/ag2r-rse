<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Group;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class GroupAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->add('change_group_status', $this->getRouterIdParameter() . '/change_status');
    }

    public function createQuery($context = 'list')
    {
        $proxyQuery = parent::createQuery($context);
        // Default Alias is "o"
        $proxyQuery->orderBy('o.createdAt', 'DESC');
        $proxyQuery->andWhere('o.status != :refused')
            ->setParameter('refused', Group::STATUS_DECLINED);

        return $proxyQuery;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')//->add('privacy')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            //->add('privacy')
            ->add('owner.fullname')
            ->add('status', null, [
                'label'    => 'Statut',
                'template' => 'AppBundle:Admin/CRUD:list_group_field_status.html.twig'
            ])
            ->add('_action', 'actions', [
                'actions' => [
                    'change_status' => [
                        'template' => 'AppBundle:Admin/CRUD:list__action_change_group_status.html.twig'
                    ],
                    'edit'          => [],
                    'delete'        => [],
                ]
            ]);
    }

    public function getAvailableStatutes(Group $group)
    {
        switch ($group->getStatus()) {
            case Group::STATUS_PENDING:
                return [Group::STATUS_CONFIRMED, Group::STATUS_DECLINED];
                break;
        }

        return [];
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('owner')
            //->add('privacy')
            ->add('description');
    }
}
