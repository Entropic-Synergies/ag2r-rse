<?php
namespace AppBundle\Menu;

use FOS\UserBundle\Model\User;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class MainBuilder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        /** @var User $user */
        $user = $this->container->get('security.context')->getToken()->getUser();

        $menu->addChild('homepage', [
            'route' => 'homepage',
            'label' => 'Accueil',
        ]);

        $messages = $menu->addChild('messages', [
            'route'   => 'fos_message_inbox',
            'label'   => 'Messages',
            'display' => false,
        ]);
        $messages->addChild('thread', [
            'abstractRoute' => 'fos_message_thread_view',
            'label'         => 'Conversation avec %displayName%',
            'object'        => 'user',
        ]);

        $ideas = $menu->addChild('ideas', [
            'route' => 'idea_index',
            'label' => 'Catalogue des idées',
        ]);
        $ideas->addChild('new', [
            'route'   => 'idea_create',
            'label'   => 'Proposer une idée',
            'display' => false,
        ]);
        $ideaShow = $ideas->addChild('show', [
            'abstractRoute' => 'idea_show',
            'display'       => false,
            'object'        => 'idea',
            'relyOn'        => 'idea',
            'label'         => '%name%',
            'routeMapping'  => [
                'idea' => 'id'
            ]
        ]);
        $ideaShow->addChild('edit', [
            'abstractRoute' => 'idea_edit',
            'display'       => false,
            'object'        => 'idea',
            'label'         => 'Edition',
        ]);
        $ideaShow->addChild('assign', [
            'abstractRoute' => 'idea_assign',
            'display'       => false,
            'object'        => 'idea',
            'label'         => 'Réaffecter',
        ]);
        $ideaShow->addChild('make_public', [
            'abstractRoute' => 'idea_make_public',
            'display'       => false,
            'object'        => 'idea',
            'label'         => 'Rendre publique',
        ]);
        $ideaShow->addChild('statute', [
            'abstractRoute' => 'idea_statute',
            'display'       => false,
            'object'        => 'idea',
            'label'         => 'Statuer',
        ]);
        $ideaShow->addChild('document', [
            'abstractRoute' => 'idea_documents',
            'display'       => false,
            'object'        => 'idea',
            'label'         => 'Documents',
        ]);

        $groups = $menu->addChild('groups', [
            'route' => 'group_index',
            'label' => 'Communautés',
        ]);
        $groups->addChild('new', [
            'abstractRoute' => 'group_new',
            'display'       => false,
            'label'         => 'Créer une communauté',
        ]);
        $groupShow = $groups->addChild('show', [
            'abstractRoute' => 'group_show',
            'object'        => 'group',
            'relyOn'        => 'group',
            'display'       => false,
            'label'         => 'Groupe %name%',
            'routeMapping'  => [
                'id' => 'id'
            ]
        ]);
        $groupShow->addChild('edit', [
            'abstractRoute' => 'group_edit',
            'display'       => false,
            'object'        => 'group',
            'label'         => 'Edition',
        ]);
        $groupShow->addChild('news', [
            'abstractRoute' => 'group_show',
            'object'        => 'group',
            'relyOn'        => 'group',
            'label'         => 'Actualités',
            'routeMapping'  => [
                'id' => 'id'
            ]
        ]);
        $groupIdeas    = $groupShow->addChild('ideas', [
            'abstractRoute' => 'group_ideas',
            'object'        => 'group',
            'relyOn'        => 'group',
            'label'         => 'Idées (%countIdeas%)',
            'routeMapping'  => [
                'id' => 'id',
            ]
        ]);
        $groupIdeaShow = $groupIdeas->addChild('group_idea_show', [
            'abstractRoute' => 'group_idea_show',
            'object'        => 'idea',
            'label'         => '%name%',
            'routeMapping'  => [
                'idea' => 'id',
            ]
        ]);

        $groupShow->addChild('users', [
            'abstractRoute' => 'group_users',
            'object'        => 'group',
            'relyOn'        => 'group',
            'label'         => 'Membres (%countUsers%)',
            'routeMapping'  => [
                'id' => 'id'
            ]
        ]);

        $groupShow->addChild('documents', [
            'abstractRoute' => 'group_documents',
            'object'        => 'group',
            'relyOn'        => 'group',
            'label'         => 'Documents (%subjectReference.countDocuments%)',
            'routeMapping'  => [
                'id' => 'id'
            ]
        ]);
        $groupShow->addChild('invite', [
            'abstractRoute' => 'group_invite',
            'object'        => 'group',
            'relyOn'        => 'group',
            'label'         => 'Inviter des membres',
            'routeMapping'  => [
                'id' => 'id'
            ]
        ]);

        $profile = $menu->addChild('profile', [
            'route'           => 'user_show',
            'routeParameters' => [
                'id' => $user->getId(),
            ],
            'label'           => 'Mon profil',
            'display'         => false,
        ]);
        $profile->addChild('profile_edit', [
            'route'   => 'fos_user_profile_edit',
            'label'   => 'Edition',
            'display' => false,
        ]);

        $users = $menu->addChild('users_search', [
            'route'   => 'search_index',
            'label'   => 'Membres',
            'display' => false,
        ]);
        $users->addChild('users_suggest', [
            'route'   => 'users_suggest',
            'label'   => 'Membres recommandés',
            'display' => false,
        ]);
        $users->addChild('user_contacts', [
            'route'   => 'user_contacts',
            'label'   => 'Contacts',
            'display' => false,
        ]);

        return $menu;
    }
}