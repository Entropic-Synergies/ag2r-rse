<?php


namespace AppBundle\Workflow;

use AppBundle\Entity\Idea;
use Lexik\Bundle\WorkflowBundle\Model\ModelInterface;

class IdeaWorkflow implements ModelInterface
{
    private $idea;

    function __construct(Idea $idea)
    {
        $this->idea = $idea;
    }

    /**
     * Returns a unique identifier.
     *
     * @return mixed
     */
    public function getWorkflowIdentifier()
    {
        return 'idea:' . $this->idea->getId();
    }

    /**
     * Returns data to store in the ModelState.
     *
     * @return array
     */
    public function getWorkflowData()
    {
        return [
            'id'            => $this->idea->getId(),
            'name'          => $this->idea->getName(),
            'description'   => $this->idea->getDescription(),
            'admin_message' => $this->idea->getAdminMessage(),
        ];
    }

    /**
     * Returns the object of the workflow.
     *
     * @return mixed
     */
    public function getWorkflowObject()
    {
        return $this->idea;
    }

    public function setStatus($status)
    {
        $this->idea->setStatus($status);
    }

    public function getStatus()
    {
        return $this->idea->getStatus();
    }
}