<?php

namespace AppBundle\Form\Handler;

use AppBundle\Entity\Document;
use AppBundle\Entity\Group;
use AppBundle\Entity\Idea;
use AppBundle\Entity\SocialReference;
use AppBundle\Entity\User;
use AppBundle\Workflow\IdeaWorkflow;
use Doctrine\ORM\EntityManager;
use ES\Bundle\BaseBundle\DependencyInjection\UserAware;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessHandlerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;

class IdeaFormHandler extends UserAware
{
    /**
     * @var ProcessHandlerInterface
     */
    private $processHandler;

    /**
     * @var EntityManager
     */
    private $em;

    function __construct(ProcessHandlerInterface $processHandler, EntityManager $em)
    {
        $this->processHandler = $processHandler;
        $this->em             = $em;
    }

    /**
     * @param FormInterface $form
     * @param Group         $group
     * @param Request       $request
     * @return Idea|bool
     */
    public function handleForm(FormInterface $form, Group $group = null, Request $request)
    {
        $isEdit = $form->getData() instanceof Idea && $form->getData()->getId() !== null;

        /** @var User $user */
        $user = $this->getUser();

        if (null === $form->getData()) {
            $idea = new Idea();
            $form->setData($idea);
            $idea->setOwner($user);
        }

        $form->handleRequest($request);
        if ($form->isValid()) {
            /** @var Idea $data */
            $data = $form->getData();
            $data->setGroup($group);

            $this->em->persist($data);
            $this->em->flush();

            if ($form->has('documents')) {
                /** @var Document[] $documents */
                $documents = $form->get('documents')->getData();
                if (!empty($documents)) {
                    /** @var SocialReference $subjectReference */
                    $subjectReference = $data->getSubjectReference();
                    foreach ($documents as $document) {
                        $document
                            ->setUser($user);
                        $subjectReference->addDocument($document);
                    }
                    $this->em->persist($subjectReference);
                    $this->em->flush();
                }
            }

            $ideaWorkflow = new IdeaWorkflow($data);
            if (!$isEdit) {
                $this->processHandler->start($ideaWorkflow);
            }
            /** @var SubmitButton $button */
            $button = $form->has('submit') ? $form->get('submit') : null;
            if ($button && $button->isClicked()) {
                $steps = ['submit'];
                if (null !== $group) {
                    $steps[] = 'accept';
                }
                foreach ($steps as $state) {
                    $modelState = $this->processHandler->reachNextState($ideaWorkflow, $state);
                    if (!$modelState->getSuccessful()) {
                        throw new \LogicException(implode(', ', $modelState->getErrors()));
                    }
                }
                $this->em->persist($data);
                $this->em->flush();
            }

            return $data;
        }

        return false;
    }
}