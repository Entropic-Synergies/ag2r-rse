<?php

namespace AppBundle\Form\Handler;

use AppBundle\Entity\Document;
use AppBundle\Entity\Idea;
use AppBundle\Entity\SocialReference;
use AppBundle\Entity\User;
use AppBundle\Workflow\IdeaWorkflow;
use Doctrine\ORM\EntityManager;
use ES\Bundle\BaseBundle\DependencyInjection\UserAware;
use ES\Bundle\SocialBundle\Manager\FeedManager;
use ES\Bundle\SocialBundle\Model\SocialObjectInterface;
use ES\Bundle\UserBundle\Model\Group;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessHandlerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;

class DocumentFormHandler extends UserAware
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FeedManager
     */
    private $feedManager;

    function __construct(EntityManager $em, FeedManager $feedManager)
    {
        $this->em          = $em;
        $this->feedManager = $feedManager;
    }

    public function handleForm(FormInterface $form, SocialObjectInterface $object = null, Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $form->handleRequest($request);
        if ($form->isValid()) {
            /** @var Document $data */
            $data = $form->getData();

            $data->setUser($user);
            /** @var SocialReference $subject */
            $subject = $object->getSubjectReference();
            $data->setObject($subject);

            $this->em->persist($data);
            $this->em->flush();

            $feed = $this->feedManager->createFeed($data->getUser(), 'add', $data, $data, $data->getObject()->getObject());
            $this->feedManager->saveFeed($feed);

            return true;
        }

        return false;
    }
}