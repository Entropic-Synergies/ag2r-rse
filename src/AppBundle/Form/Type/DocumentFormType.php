<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nom du document',
            ])
            ->add('file', 'es_file', [
                'ajax'                 => true,
                'label'                => 'Fichier',
                'upload_route_name'    => 'idea_document_upload',
                'display_preview_name' => true,
                'accept'               => null,
            ]);

        if ($options['include_submit']) {
            $builder->add('submit', 'submit', [
                'label' => 'Ajouter le document',
            ]);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class'     => 'AppBundle\Entity\Document',
            'include_submit' => true,
        ]);
    }

    public function getName()
    {
        return 'document_form';
    }
}