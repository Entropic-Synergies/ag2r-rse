<?php

namespace AppBundle\Form\Type;

use ES\Bundle\UserBundle\Form\Type\ProfileFormType as ProfileFormTypeBase;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileFormType extends ProfileFormTypeBase
{
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', null, [
                'label'              => 'form.lastname',
                'translation_domain' => 'FOSUserBundle',
                //'attr'               => ['placeholder' => 'form.placeholder.lastname'],
            ])
            ->add('firstname', null, [
                'label'              => 'form.firstname',
                'translation_domain' => 'FOSUserBundle',
                //'attr'               => ['placeholder' => 'form.placeholder.firstname'],
            ])
            ->add('email', null, [
                'label'              => 'form.email',
                'translation_domain' => 'FOSUserBundle',
                // 'attr'               => ['placeholder' => 'form.placeholder.email'],
            ])
            ->add('phone', null, [
                'label'              => 'form.phone',
                'translation_domain' => 'FOSUserBundle',
                // 'attr'               => ['placeholder' => 'form.placeholder.phone'],
            ])
            ->add('description', null, [
                'label'              => 'form.description',
                'translation_domain' => 'FOSUserBundle',
                'attr'               => [
                    'rows'        => 10,
                    'placeholder' => 'form.placeholder.description'
                ],
            ])
            ->add('picture', 'es_file', [
                'ajax'               => true,
                'label'              => 'form.picture',
                'translation_domain' => 'FOSUserBundle',
                'upload_route_name'  => 'profile_picture_upload',
            ])
            ->add('interests', 'es_select2_orm', [
                'multiple'     => true,
                'class'        => 'AppBundle\Entity\Interest',
                'free_entries' => true,
            ])
            ->add('termsAccepted', null, [
                'label'              => 'form.terms',
                'translation_domain' => 'FOSUserBundle',
            ]);
    }
}
