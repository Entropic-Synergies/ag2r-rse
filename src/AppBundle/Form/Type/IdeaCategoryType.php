<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use ES\Bundle\UserBundle\Model\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IdeaCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Titre',
            ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\IdeaCategory',
        ]);
    }

    public function getName()
    {
        return 'idea_category';
    }
}