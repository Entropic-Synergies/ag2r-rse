<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use ES\Bundle\UserBundle\Model\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\RouterInterface;

class AssignIdeaFormType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'es_select2_orm', [
                'class' => 'AppBundle\Entity\User',
                'label' => 'Nouvel opérateur',
                'url'   => $this->router->generate('users_ajax'),
            ])
            ->add('submit', 'submit', [
                'label' => 'Réaffecter l\'idée',
            ]);
    }

    public function getName()
    {
        return 'assign_idea_form';
    }
}