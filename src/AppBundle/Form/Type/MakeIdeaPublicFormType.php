<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Idea;
use Doctrine\ORM\EntityRepository;
use ES\Bundle\UserBundle\Model\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MakeIdeaPublicFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('category', null, [
            'label'         => 'Thématique',
            'empty_value'   => 'Sélectionnez une thématique publique...',
            'query_builder' => function (EntityRepository $repository) use (&$options) {
                $queryBuilder = $repository->createQueryBuilder('c')
                    ->andWhere('c.group IS NULL');

                return $queryBuilder;
            }
        ]);

        $builder
            ->add('submit', 'submit', [
                'label' => 'Rendre publique',
            ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Idea',
        ]);
    }

    public function getName()
    {
        return 'make_idea_public_form';
    }
}