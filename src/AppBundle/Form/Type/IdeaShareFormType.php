<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Idea;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\RouterInterface;

class IdeaShareFormType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    protected $router;

    function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Idea $idea */
        $idea = $options['idea'];
        $builder
            ->add('users', 'es_select2_orm', [
                'class'    => 'AppBundle\Entity\User',
                'multiple' => true,
                'url'      => $this->router->generate('idea_share_users', ['idea' => $idea->getId()]),
                'label'    => 'Partager à',
            ])
            ->add('message', 'textarea', [
                'label' => 'Message',
                'attr'  => [
                    'rows'        => 3,
                    'placeholder' => 'Accompagnez votre partage d\'une note...',
                ]
            ]);

        $builder
            ->add('submit', 'submit', [
                'label' => 'Partager',
            ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Model\IdeaShare',
        ]);

        $resolver->setRequired(['idea']);

        $resolver->setAllowedTypes([
            'idea' => [
                'AppBundle\Entity\Idea',
            ]
        ]);
    }

    public function getName()
    {
        return 'idea_share_form';
    }
}