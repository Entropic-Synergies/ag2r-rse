<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Idea;
use AppBundle\Workflow\IdeaWorkflow;
use Lexik\Bundle\WorkflowBundle\Flow\Process;
use Lexik\Bundle\WorkflowBundle\Flow\Step;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessAggregator;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessHandlerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StatuteIdeaFormType extends AbstractType
{
    /**
     * @var ProcessHandlerInterface
     */
    protected $workflow;

    /**
     * @var ProcessAggregator
     */
    protected $processAggregator;

    function __construct(ProcessAggregator $processAggregator, ProcessHandlerInterface $workflow)
    {
        $this->processAggregator = $processAggregator;
        $this->workflow          = $workflow;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Idea $idea */
        $idea = $options['idea'];

        /** @var Step[] $availableNextSteps */
        $availableNextSteps = $this->getAvailableNextSteps($options['idea']);
        $statutes           = [];
        foreach ($availableNextSteps as $availableNextStep) {
            $statutes[$availableNextStep->getName()] = 'idea.actions.' . $idea->getStatus() . '.' . $availableNextStep->getName();
        }

        $builder
            ->add('status', 'choice', [
                'choices' => $statutes,
                'mapped' => false,
            ])
            ->add('adminMessage', 'textarea', [
                'required' => false,
                'label'    => 'Motif',
            ])
            ->add('submit', 'submit', [
                'label' => 'Statuer',
            ]);
    }

    private function getAvailableNextSteps(Idea $idea)
    {
        $model      = new IdeaWorkflow($idea);
        $modelState = $this->workflow->getCurrentState($model);

        /** @var Process $process */
        $process = $this->processAggregator->getProcess('idea_publication');
        /** @var Step $step */
        $step       = $process->getStep($modelState->getStepName());
        $nextStates = $step->getNextStates();

        return $nextStates;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(['idea']);
        $resolver->setAllowedTypes(['idea' => ['AppBundle\Entity\Idea']]);
    }

    public function getName()
    {
        return 'statute_idea_form';
    }
}