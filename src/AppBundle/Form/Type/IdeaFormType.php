<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Idea;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use ES\Bundle\UserBundle\Model\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;

class IdeaFormType extends AbstractType
{
    /**
     * @var SecurityContextInterface
     */
    private $securityContext;

    function __construct(SecurityContextInterface $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Titre de l\'idée',
            ])
            ->add('category', null, [
                'label'         => 'Thématique',
                'empty_value'   => 'Sélectionnez...',
                'query_builder' => function (EntityRepository $repository) use (&$options) {
                    $queryBuilder = $repository->createQueryBuilder('c');
                    if ($options['group'] instanceof Group) {
                        $queryBuilder->andWhere('c.group = :group')
                            ->setParameter('group', $options['group']->getId());
                    } else {
                        $queryBuilder->andWhere('c.group IS NULL');
                    }

                    return $queryBuilder;
                }
            ])
            ->add('description', null, [
                'label' => 'Description',
                'attr'  => [
                    'rows'        => 10,
                    'placeholder' => 'Décrivez en quelques mots votre idée',
                ]
            ]);

        /** @var Idea $data */
        if (null === $data = $builder->getData()) {
            $builder->add('documents', 'collection', [
                'mapped'  => false,
                'type'    => new DocumentFormType(),
                'options' => [
                    'include_submit' => false,
                ]
            ]);
        }
        if (null === $data || $data->getStatus() === Idea::STATUS_DRAFT) {
            $builder
                ->add('save_draft', 'submit', [
                    'label' => 'Enregistrer en brouillon',
                ])->add('submit', 'submit', [
                    'label' => 'Proposer l\'idée',
                ]);
        } else {
            $builder
                ->add('edit', 'submit', [
                    'label' => 'Modifier',
                ]);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class'         => 'AppBundle\Entity\Idea',
            'group'              => null,
            'cascade_validation' => true,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        /** @var User $user */
        $user = $this->securityContext->getToken()->getUser();
        $view->vars['attr']['terms_not_accepted'] = !$user->isTermsAccepted();
    }

    public function getName()
    {
        return 'idea_form';
    }
}