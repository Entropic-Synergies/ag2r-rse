<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use ES\Bundle\UserBundle\Model\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GroupCategoriesFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categories', 'collection', [
                'type'  => 'idea_category',
                'label' => 'Thématiques',
            ])
            ->add('submit', 'submit', [
                'label' => 'Sauvegarder',
            ]);
    }

    public function getName()
    {
        return 'group_categories_form';
    }
}