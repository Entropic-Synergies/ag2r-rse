<?php


namespace AppBundle\Consumer;

use AppBundle\Entity\Group;
use Doctrine\ORM\EntityManager;
use ES\Bundle\SocialBundle\Model\TimelineInterface;
use ES\Bundle\UserBundle\Event\GroupEvent;
use Sonata\NotificationBundle\Consumer\ConsumerEvent;
use Sonata\NotificationBundle\Consumer\ConsumerInterface;

class GroupConsumer implements ConsumerInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Process a ConsumerEvent
     *
     * @param ConsumerEvent $event
     */
    public function process(ConsumerEvent $event)
    {
        $body = $event->getMessage()->getBody();

        $groupId = $body['group'];

        $group = $this->em->getRepository('AppBundle:Group')->find($groupId);

        if ($group instanceof Group) {
            if ($body['way'] === GroupEvent::PRIVACY_OPEN_TO_CLOSED) {
                $timelines = $this->em->createQueryBuilder()
                    ->select('t')
                    ->from('ES\Bundle\SocialBundle\Model\Timeline', 't')
                    ->join('t.feed', 'f')
                    ->andWhere('f.context = :group_subject')
                    ->andWhere('t.subject != :group_subject')
                    ->setParameter('group_subject', $group->getSubjectReference()->getId())
                    ->getQuery()
                    ->getResult();

                /** @var TimelineInterface[] $timelines */
                foreach ($timelines as $timeline) {
                    $this->em->remove($timeline);
                }
                $this->em->flush();
            } elseif ($body['way'] === GroupEvent::PRIVACY_CLOSED_TO_OPEN) {
                // TODO regenerate feeds
            }
        }
    }
}