<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Idea;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use ES\Bundle\UserBundle\Model\Group;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;

class IdeaRepository extends EntityRepository
{
    public function getPreviousIdea(Idea $idea)
    {
        $queryBuilder = $this->createQueryBuilder('i')
            ->where('i.id > :id')
            ->setParameter('id', $idea->getId())
            ->addOrderBy('i.id', 'ASC');

        if (null !== $group = $idea->getGroup()) {
            $queryBuilder
                ->andWhere('i.group = :group')
                ->setParameter('group', $group->getId());
        } else {
            $queryBuilder->andWhere('i.group IS NULL');
        }

        return $queryBuilder
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getNextIdea(Idea $idea)
    {
        $queryBuilder = $this->createQueryBuilder('i')
            ->where('i.id < :id')
            ->setParameter('id', $idea->getId())
            ->addOrderBy('i.id', 'DESC');

        if (null !== $group = $idea->getGroup()) {
            $queryBuilder
                ->andWhere('i.group = :group')
                ->setParameter('group', $group->getId());
        } else {
            $queryBuilder->andWhere('i.group IS NULL');
        }

        return $queryBuilder
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getAvailableStatutes(Group $group = null)
    {
        $statutes = [
            Idea::STATUS_DRAFT,
            Idea::STATUS_STUDY_PENDING,
            Idea::STATUS_STUDY,
            Idea::STATUS_EXPERIMENTATION,
            Idea::STATUS_ESTIMATION,
            Idea::STATUS_IMPLEMENTED,
        ];

        return $statutes;
    }

    public function getCategories(Group $group = null)
    {
        return $this->_em->getRepository('AppBundle:IdeaCategory')->findBy([
            'group' => null !== $group ? $group->getId() : null,
        ]);
    }

    public function getIdeasQueryBuilder(User $user, array $filters, $order = null, Group $group = null)
    {
        $availableStatutes = $this->getAvailableStatutes($group);
        if (isset($filters['status']) && in_array($filters['status'], $availableStatutes)) {
            $statutes = [$filters['status']];
        } else {
            $statutes = $availableStatutes;
        }

        $queryBuilder = $this->createQueryBuilder('i');
        $queryBuilder
            ->leftJoin('i.group', 'g')
            ->leftJoin('g.users', 'gu')
            ->where('i.status IN (:statutes)')
            ->setParameter('statutes', $statutes)
            ->andWhere('i.status != :status_draft OR i.owner = :user')
            ->setParameter('status_draft', Idea::STATUS_DRAFT)
            ->setParameter('user', $user->getId());

        switch ($order) {
            case 'popular':
                $queryBuilder
                    ->addSelect('sr.upVotesCount - sr.downVotesCount AS HIDDEN score')
                    ->innerJoin('i.subjectReference', 'sr')
                    ->addOrderBy('score', 'DESC');
                break;
            default:
            case 'recent':
                $queryBuilder->addOrderBy('i.id', 'DESC');
                break;
        }

        if (isset($filters['category']) && $filters['category']) {
            $queryBuilder->andWhere('i.category = :category')
                ->setParameter('category', $filters['category']);
        }
        if (isset($filters['mine']) && $filters['mine']) {
            $queryBuilder->leftJoin('i.operators', 'o')
                ->andWhere('o.id = :user OR i.owner = :user');
        }

        if (null !== $group) {
            $queryBuilder->andWhere('i.group = :group')
                ->setParameter('group', $group->getId());
        } elseif (isset($filters['group']) && $filters['group']) {
            if ($filters['group'] === 'private') {
                $queryBuilder->andWhere('gu.user = :user');
            } elseif ($filters['group'] === 'public') {
                $queryBuilder->andWhere('i.group IS NULL');
            } else {
                $queryBuilder->andWhere('i.group = :group')
                    ->andWhere('gu.user = :user')
                    ->setParameter('group', $filters['group']);
            }
        } else {
            $queryBuilder->andWhere('gu.user = :user OR i.group IS NULL');
        }

        return $queryBuilder;
    }

    public function paginate(QueryBuilder $queryBuilder, Request $request)
    {
        $adapter    = new DoctrineORMAdapter($queryBuilder, true, false);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);
        $pagerfanta->setCurrentPage($request->get('page', 1));

        return $pagerfanta;
    }
} 