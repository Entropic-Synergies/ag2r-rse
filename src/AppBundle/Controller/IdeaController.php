<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Group;
use AppBundle\Entity\Idea;
use AppBundle\Entity\User;
use AppBundle\Form\Type\MakeIdeaPublicFormType;
use AppBundle\Model\IdeaShare;
use AppBundle\Repository\IdeaRepository;
use Doctrine\ORM\QueryBuilder;
use ES\Bundle\BaseBundle\Controller\Select2AjaxDoctrineControllerTrait;
use ES\Bundle\UserBundle\Controller\UtilsTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class IdeaController extends Controller
{
    use UtilsTrait;
    use DocumentTrait;
    use Select2AjaxDoctrineControllerTrait;

    /**
     * @Route("/ideas", name="idea_index")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        /** @var IdeaRepository $repo */
        $repo              = $this->getDoctrine()->getRepository('AppBundle:Idea');
        $availableStatutes = $repo->getAvailableStatutes();

        $ideaCategories = $repo->getCategories();

        // Filter interactions
        $filters = $request->get('filters', []);
        $order   = $request->get('order', []);

        if (isset($filters['category'])) {
            if (!empty($filters['category'])) {
                $filters['group'] = null;
            }
            if (!empty($filters['group'])) {
                $filters['category'] = null;
            }
        }

        /** @var User $user */
        $user         = $this->getUser();
        $queryBuilder = $repo->getIdeasQueryBuilder($user, $filters, $order);
        $pagerfanta   = $repo->paginate($queryBuilder, $request);

        $userGroups = $this->get('es_user.group_manager')->getUserGroups($user);

        return [
            'statutes'       => $availableStatutes,
            'pager'          => $pagerfanta,
            'ideas'          => $pagerfanta->getCurrentPageResults(),
            'ideaCategories' => $ideaCategories,
            'userGroups'     => $userGroups,
        ];
    }

    /**
     * @Route("/ideas/new", name="idea_create")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm('idea_form');

        $formHandler = $this->get('ag2r.form.handler.idea');
        if (false !== $data = $formHandler->handleForm($form, null, $request)) {
            if ($data->getStatus() === Idea::STATUS_DRAFT) {
                $this->addFlash('success', 'Votre idée a bien été enregistrée dans vos brouillons');
            } else {
                $this->addFlash('success', 'Votre idée a bien été proposée');
            }

            return $this->redirect($this->generateUrl('homepage'));
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/ideas/{idea}/edit", name="idea_edit")
     * @Template()
     */
    public function editAction(Idea $idea, Request $request)
    {
        if (!$this->get('security.context')->isGranted('EDIT', $idea)) {
            throw new AccessDeniedHttpException();
        }

        $wasSubmitted = $idea->getStatus() !== Idea::STATUS_DRAFT;

        $form = $this->createForm('idea_form', $idea, [
            'group' => $idea->getGroup(),
        ]);

        $formHandler = $this->get('ag2r.form.handler.idea');
        if (false !== $data = $formHandler->handleForm($form, null, $request)) {
            if ($wasSubmitted) {
                $this->addFlash('success', 'Votre idée a bien été modifiée');
            } elseif ($data->getStatus() === Idea::STATUS_DRAFT) {
                $this->addFlash('success', 'Votre idée a bien été enregistrée dans vos brouillons');
            } else {
                $this->addFlash('success', 'Votre idée a bien été proposée');
            }

            return $this->redirect($this->generateUrl('idea_show', ['idea' => $idea->getId()]));
        }

        return [
            'idea' => $idea,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/ideas/{idea}/make-public", name="idea_make_public")
     * @Template()
     */
    public function makePublicAction(Idea $idea, Request $request)
    {
        if (!$this->get('security.context')->isGranted('EDIT', $idea)) {
            throw new AccessDeniedHttpException();
        }

        if (null === $idea->getGroup()) {
            throw new NotFoundHttpException('Idea is not in a group');
        }

        $form = $this->createForm(new MakeIdeaPublicFormType(), $idea);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $idea->getGroup()->updateIdeasCount(-1);
            $idea->setGroup(null);
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($idea);
            $em->flush();

            $this->addFlash('success', 'L\'idée a bien été rendue publique');
            return $this->redirect($this->generateUrl('idea_show', ['idea' => $idea->getId()]));
        }

        return [
            'idea' => $idea,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/ideas/{idea}/delete", name="idea_delete")
     */
    public function deleteAction(Idea $idea, Request $request)
    {
        if (!$this->get('security.context')->isGranted('DELETE', $idea)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($idea);
        $em->flush();

        $this->addFlash('success', 'L\'idée a bien été supprimée');

        return $this->redirect($this->generateUrl('idea_index'));
    }

    /**
     * @Route("/ideas/{idea}/share", name="idea_share")
     */
    public function shareAction(Idea $idea, Request $request)
    {
        $form = $this->createForm('idea_share_form', null, [
            'idea' => $idea,
        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            /** @var IdeaShare $share */
            $share = $form->getData();

            $notificationManager = $this->get('es_notification.notification_manager');
            /** @var User $user */
            $user         = $this->getUser();
            $notification = $notificationManager->createNotification($user->getSubjectReference(), 'share', $idea->getSubjectReference());
            $notification->setMetadata([
                'message' => $share->getMessage(),
            ]);
            $notificationManager->notifyUsers($notification, $share->getUsers()->toArray());

            $this->addFlash('success', 'L\'idée a bien été partagée');

            return $this->redirect($this->generateUrl('idea_show', ['idea' => $idea->getId()]));
        }

        return $this->render('AppBundle:Idea:share.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ideas/{idea}/share/users", name="idea_share_users")
     */
    public function shareUsersAction(Idea $idea, Request $request)
    {
        $group = $idea->getGroup();

        return $this->select2DoctrineAjaxAction('AppBundle\Entity\User', function (User $user) {
            return [
                'id'   => $user->getId(),
                'text' => $user->getFullname(),
            ];
        }, ['firstname',
            'lastname'], function (QueryBuilder $queryBuilder) use ($group) {
            $queryBuilder
                ->innerJoin('ES\Bundle\UserBundle\Model\Friend', 'f', 'WITH', 'f.friend = t.id AND f.user = :user')
                ->setParameter('user', $this->getUser());

            if (null !== $group) {
                $queryBuilder
                    ->innerJoin('ES\Bundle\UserBundle\Model\GroupUser', 'gu', 'WITH', 'gu.user = t.id AND gu.group = :group')
                    ->setParameter('group', $group->getId());
            }
        });
    }

    /**
     * @Route("/ideas/{idea}/documents", name="idea_documents")
     * @Template()
     */
    public function documentsAction(Idea $idea, Request $request)
    {
        $params = [
            'documents' => $this->getDocuments($idea),
            'idea'      => $idea,
        ];

        if ($this->get('security.context')->isGranted('EDIT', $idea)) {
            $form = $this->handleForm($request, $idea);
            if ($form->isValid()) {
                return $this->redirect($this->generateUrl('idea_documents', ['idea' => $idea->getId()]));
            }
            $params['documentForm'] = $form->createView();
        }

        return $params;
    }

    /**
     * @Route("/ideas/{idea}", name="idea_show")
     */
    public function showAction(Idea $idea)
    {
        if (!$this->isGranted('VIEW', $idea)) {
            throw new AccessDeniedHttpException();
        }

        /** @var IdeaRepository $repo */
        $repo = $this->getDoctrine()->getRepository('AppBundle:Idea');

        $previousIdea = $idea;
        do {
            $previousIdea = $repo->getPreviousIdea($previousIdea);
        } while (null !== $previousIdea && !$this->isGranted('VIEW', $previousIdea));

        $nextIdea = $idea;
        do {
            $nextIdea = $repo->getNextIdea($nextIdea);
        } while (null !== $nextIdea && !$this->isGranted('VIEW', $nextIdea));

        return $this->render('AppBundle:Idea:show.html.twig', [
            'idea'         => $idea,
            'previousIdea' => $previousIdea,
            'nextIdea'     => $nextIdea,
        ]);
    }

    /**
     * @Route("/groups/{group}/ideas/{idea}", name="group_idea_show")
     */
    public function groupShowAction(Group $group, Idea $idea)
    {
        return $this->showAction($idea);
    }

    /**
     * @Route("/ideas/{idea}/assign", name="idea_assign")
     * @Template()
     */
    public function assignAction(Idea $idea, Request $request)
    {
        if (!$this->isGranted('OPERATOR', $idea)) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->createForm('assign_idea_form');
        $form->handleRequest($request);
        if ($form->isValid()) {
            /** @var User $user */
            $user     = $this->getUser();
            $operator = $form->get('user')->getData();

            if (!$operator instanceof User) {
                throw new \InvalidArgumentException('Invalid user');
            }

            $aclManager = $this->get('es_base.security.acl.manager');
            $idea->addOperator($operator);
            $idea->removeOperator($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($idea);
            $em->flush();

            $aclManager->grantUserOnObject($operator, $idea, MaskBuilder::MASK_OPERATOR);
            $aclManager->revokeUserOnObject($user, $idea, MaskBuilder::MASK_OPERATOR);

            $notificationManager = $this->get('es_notification.notification_manager');
            $notification        = $notificationManager->createNotification(
                $user->getSubjectReference(),
                'reassign_operator',
                $idea->getSubjectReference(),
                $idea->getSubjectReference()
            );
            $notificationManager->notifyUsers($notification, [$operator]);

            $this->addFlash('success', sprintf('L\'idée a bien été réaffectée à %s', $operator->getFullname()));

            return $this->redirect($this->generateUrl('idea_index'));
        }

        return [
            'idea' => $idea,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/ideas/{idea}/statute", name="idea_statute")
     * @Template()
     */
    public function statuteAction(Idea $idea, Request $request)
    {
        if (!$this->isGranted('OPERATOR', $idea)) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->createForm('statute_idea_form', $idea, ['idea' => $idea]);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $status = $form->get('status')->getData();

            $ideaManager = $this->get('ag2r.idea.manager');
            /** @var User $user */
            $user = $this->getUser();
            $ideaManager->reachNextState($idea, $status, $user);

            $this->addFlash('success', 'L\'idée a bien statuée');

            return $this->redirect($this->generateUrl('idea_index'));
        }

        return [
            'idea' => $idea,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/file/upload", name="idea_document_upload")
     */
    public function uploadAction()
    {
        $fileManager = $this->get('es_fileupload.file_upload_manager');

        return $fileManager->handleForm(null, [
            'accept' => null,
        ]);
    }
}
