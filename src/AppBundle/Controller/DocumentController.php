<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use AppBundle\Entity\Idea;
use Cocur\Slugify\Slugify;
use ES\Bundle\UserBundle\Controller\UtilsTrait;
use ES\Bundle\UserBundle\Model\Group;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DocumentController extends Controller
{
    use UtilsTrait;
    use DocumentTrait;

    /**
     * @Route("/document/{document}/download/{hash}", name="document_download")
     */
    public function downloadAction(Document $document, $hash)
    {
        $object = $document->getObject()->getObject();
        if ($document->getHash() !== $hash) {
            throw new NotFoundHttpException;
        }
        if (!$this->isGranted('VIEW', $document) && !$this->isGranted('VIEW', $object)) {
            throw new AccessDeniedHttpException();
        }

        $path = $document->getFile()->getPath();

        $response = new BinaryFileResponse($path);
        $response->headers->set('Content-Type', $document->getFile()->getMimeType());
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            Slugify::create()->slugify($document->getName()) . '.' . $document->getFile()->getExtension()
        );

        return $response;
    }

    /**
     * @Route("/document/{document}/delete", name="document_delete", requirements={"_method"="POST"})
     */
    public function deleteDocument(Document $document)
    {
        $object = $document->getObject()->getObject();
        if (!$this->isGranted('DELETE', $document)
            && !$this->isGranted('OPERATOR', $object)
        ) {
            throw new AccessDeniedHttpException;
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($document);
        $em->flush();

        return new JsonResponse(true);
    }
}
