<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Group;
use AppBundle\Entity\Idea;
use AppBundle\Entity\IdeaCategory;
use AppBundle\Entity\User;
use AppBundle\Form\Type\DocumentFormType;
use AppBundle\Repository\IdeaRepository;
use Doctrine\ORM\QueryBuilder;
use ES\Bundle\UserBundle\Controller\UtilsTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class GroupController extends Controller
{
    use UtilsTrait;
    use DocumentTrait;

    /**
     * @Route("/groups", name="group_index")
     * @Template()
     */
    public function indexAction()
    {
        $groupManager = $this->get('es_user.group_manager');
        $user         = $this->getUser();
        $limit        = 40;
        $offset       = 0;

        $myGroupsQB = $groupManager->getQueryBuilder();
        $myGroupsQB
            ->andWhere('g.owner = :user')
            ->andWhere('g.status = :confirmed')
            ->setParameter('confirmed', Group::STATUS_CONFIRMED)
            ->setParameter('user', $user->getId());
        $myGroups = $this->getResult($myGroupsQB);

        $userGroupsQB = $groupManager->getQueryBuilder();
        $userGroupsQB->innerJoin('g.users', 'gu')
            ->andWhere('g.status = :confirmed')
            ->setParameter('confirmed', Group::STATUS_CONFIRMED)
            ->andWhere('gu.user = :user')
            ->andWhere('g.owner != :user')
            ->setParameter('user', $user->getId());
        $userGroups = $this->getResult($userGroupsQB);

        $groupsQB = $groupManager->getQueryBuilder();
        $groupsQB->leftJoin('g.users', 'gu', 'WITH', 'gu.user = :user')
            ->andWhere('g.status = :confirmed')
            ->setParameter('confirmed', Group::STATUS_CONFIRMED)
            ->andWhere('gu.id IS NULL')
            ->andWhere('g.privacy < 2')
            ->andWhere('g.owner != :user')
            ->setParameter('user', $user->getId());
        $groups = $this->getResult($groupsQB, $limit, $offset);

        return [
            'myGroups'   => $myGroups,
            'userGroups' => $userGroups,
            'groups'     => $groups,
        ];
    }

    private function getResult(QueryBuilder $queryBuilder, $limit = null, $offset = null)
    {
        if (null !== $limit) {
            $queryBuilder->setMaxResults($limit);
        }
        if (null !== $offset) {
            $queryBuilder->setFirstResult($offset);
        }

        return $queryBuilder
            ->getQuery()
            ->getResult();
    }

    /**
     * @Route("/groups/new", name="group_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm('es_user_group_form');

        $form->handleRequest($request);
        if ($form->isValid()) {
            /** @var Group $group */
            $group = $form->getData();
            $group->setOwner($this->getUser());

            $em = $this->getDoctrine()->getManager();
            if ($this->isGranted('ROLE_ADMIN')) {
                $group->setStatus(Group::STATUS_CONFIRMED);
            }
            $em->persist($group);
            $em->flush();

            $this->get('es_user.group_manager')->join($group);

            if ($this->isGranted('ROLE_ADMIN')) {
                $this->addFlash('success', 'La communauté a bien été créée');

                return $this->redirect($this->generateUrl('group_show', ['id' => $group->getId()]));
            } else {
                $this->addFlash('success', 'La communauté a bien été envoyée en validation.');

                return $this->redirect($this->generateUrl('group_index'));
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/groups/{id}/edit", name="group_edit")
     * @Template()
     */
    public function editAction(Group $group, Request $request)
    {
        $form = $this->createForm('es_user_group_form', $group);

        if (!$this->isGranted('EDIT', $group)) {
            throw new AccessDeniedHttpException();
        }

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            $this->addFlash('success', 'La communauté a bien été mise à jour');

            return $this->redirect($this->generateUrl('group_show', ['id' => $group->getId()]));
        }

        return [
            'group' => $group,
            'form'  => $form->createView(),
        ];
    }

    /**
     * @Route("/groups/{id}/categories/manage", name="group_categories_manage")
     * @Template()
     */
    public function manageCategoriesAction(Group $group, Request $request)
    {
        if (!$this->isGranted('OPERATOR', $group)) {
            throw new AccessDeniedHttpException();
        }
        $repoIdea = $this->getDoctrine()->getRepository('AppBundle:Idea');
        /** @var IdeaCategory[] $categories */
        $categories = $repoIdea->getCategories($group);
        $form       = $this->createForm('group_categories_form', ['categories' => $categories]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /** @var IdeaCategory[] $submittedCategories */
            $submittedCategories = $form->get('categories')->getData();

            foreach ($submittedCategories as $category) {
                $category->setGroup($group);
                $em->persist($category);
            }

            foreach ($categories as $category) {
                $found = false;
                foreach ($submittedCategories as $submittedCategory) {
                    if ($submittedCategory->getId() === $category->getId()) {
                        $found = true;
                    }
                }
                if (false === $found) {
                    $idea = $repoIdea->findOneBy([
                        'category' => $category->getId(),
                    ]);
                    if ($idea instanceof Idea) {
                        $this->addFlash('error', sprintf('Impossible de supprimer la thématique %s car elle contient des idées.', $category->getName()));

                        return $this->redirect($this->generateUrl('group_categories_manage', ['id' => $group->getId()]));
                    } else {
                        $em->remove($category);
                    }
                }
            }

            $em->flush();

            $this->addFlash('success', 'Les thématiques ont été mises à jour');

            return $this->redirect($this->generateUrl('group_show', ['id' => $group->getId()]));
        }

        return [
            'group' => $group,
            'form'  => $form->createView(),
        ];
    }

    /**
     * @Route("/groups/{id}", name="group_show")
     * @Template()
     */
    public function showAction(Group $group, Request $request)
    {
        /** @var FormInterface $feedForm */
        $params = $this->getShowParameters($group, $feedForm);

        $feeds = $this->get('es_social.feed_manager')->getTimelineFeeds($group->getSubjectReference(), $this->get('request'));

        if ($request->isXmlHttpRequest()) {
            return $this->render('AppBundle:Feed:feedsContent.html.twig', [
                'feeds'   => $feeds,
                'context' => $group->getSubjectReference(),
            ]);
        }

        $feedFormHandler = $this->get('es_social.form.feed');

        if ($feedFormHandler->handleForm($feedForm, $request)) {
            $this->addFlash('success', 'Votre message a bien été publié');

            return $this->redirect($this->generateUrl('group_show', ['id' => $group->getId()]));
        }

        $params = array_merge($params, [
            'feeds' => $feeds,
        ]);

        $this->createFormViews($params);

        return $params;
    }

    private function getShowParameters(Group $group, &$feedForm = null, &$ideaForm = null, &$documentForm = null)
    {
        $groupManager = $this->get('es_user.group_manager');

        $hasAccess = $this->isGranted('VIEW', $group);

        $groupInvite = $groupManager->getInvite($group);

        if (!$hasAccess && $group->isSecret() && !$groupInvite) {
            throw new AccessDeniedHttpException();
        }
        if (!$group->isConfirmed() && !$this->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException;
        }

        $ideaForm = $this->createForm('idea_form', null, [
            'group'  => $group,
            'action' => $this->generateUrl('group_ideas', ['id' => $group->getId()]),
        ]);

        $documentForm = $this->createForm(new DocumentFormType(), null, [
            'action' => $this->generateUrl('group_documents', ['id' => $group->getId()]),
        ]);

        $feedFormHandler = $this->get('es_social.form.feed');
        $feedForm        = $feedFormHandler->getForm(null, $group->getSubjectReference(), [
            'action'           => $this->generateUrl('group_show', ['id' => $group->getId()]),
            'pictures_enabled' => false,
        ]);
        $feedForm->add('submit', 'submit', [
            'label' => 'Publier',
        ]);

        $groupRequest = $groupManager->getAccess($group);
        $groupInvite  = null === $groupRequest ? $groupInvite : null;

        $params = [
            'group'        => $group,
            'groupRequest' => $groupRequest,
            'groupInvite'  => $groupInvite,
            'hasAccess'    => $hasAccess,
            'feedForm'     => $feedForm,
            'ideaForm'     => $ideaForm,
            'documentForm' => $documentForm,
        ];

        return $params;
    }

    private function createFormViews(array &$params)
    {
        foreach ($params as &$param) {
            if ($param instanceof FormInterface) {
                $param = $param->createView();
            }
        }
    }

    /**
     * @Route("/groups/{id}/users", name="group_users")
     * @Template()
     */
    public function usersAction(Group $group)
    {
        $params = $this->getShowParameters($group);

        $groupManager = $this->get('es_user.group_manager');

        if ($this->get('security.context')->isGranted('EDIT', $group) || $group->isOpen()) {
            if ($group->countPendingGroupInvites()) {
                $params['pendingInvites'] = $groupManager->getPendingInvitesQuery($group)->getResult();
            }
            if ($group->countPendingGroupRequests()) {
                $params['pendingRequests'] = $groupManager->getPendingRequestsQuery($group)->getResult();
            }
        }

        $this->createFormViews($params);

        return $params;
    }

    /**
     * @Route("/groups/{id}/invite", name="group_invite")
     * @Template()
     */
    public function inviteAction(Group $group, Request $request)
    {
        $params = $this->getShowParameters($group);

        $form = $this->createForm('es_user_group_invite_form', null, [
            'group' => $group,
        ]);

        $formHandler = $this->get('es_user.form.group_invite.handler');
        $response    = $formHandler->handle($form, $request);
        if ($response instanceof Response) {
            return $response;
        }

        $params['inviteForm'] = $form;

        $this->createFormViews($params);

        return $params;
    }

    /**
     * @Route("/groups/{id}/ideas", name="group_ideas")
     * @Template()
     */
    public function ideasAction(Group $group, Request $request)
    {
        $params = $this->getShowParameters($group, $feedForm, $form);

        /** @var IdeaRepository $repo */
        $repo = $this->getDoctrine()->getRepository('AppBundle:Idea');

        $filters = $request->get('filters', []);
        $order   = $request->get('order', []);

        /** @var User $user */
        $user = $this->getUser();

        $queryBuilder = $repo->getIdeasQueryBuilder($user, $filters, $order, $group);
        $pagerfanta   = $repo->paginate($queryBuilder, $request);

        $formHandler = $this->get('ag2r.form.handler.idea');
        if (false !== $data = $formHandler->handleForm($form, $group, $request)) {
            if ($data->getStatus() === Idea::STATUS_DRAFT) {
                $this->addFlash('success', 'Votre idée a bien été enregistrée dans vos brouillons');
            } else {
                $this->addFlash('success', 'Votre idée a bien été proposée');
            }

            return $this->redirect($this->generateUrl('group_ideas', ['id' => $group->getId()]));
        }

        $params = array_merge($params, [
            'pager'          => $pagerfanta,
            'ideas'          => $pagerfanta->getCurrentPageResults(),
            'statutes'       => $repo->getAvailableStatutes($group),
            'ideaCategories' => $repo->getCategories($group),
        ]);

        $this->createFormViews($params);

        return $params;
    }

    /**
     * @Route("/groups/{id}/documents", name="group_documents")
     * @Template()
     */
    public function documentsAction(Group $group, Request $request)
    {
        $params = $this->getShowParameters($group, $feedForm, $ideaForm, $documentForm);

        $formHandler = $this->get('ag2r.form.handler.document');
        if (false !== $data = $formHandler->handleForm($documentForm, $group, $request)) {
            $this->addFlash('success', 'Le document a bien été ajouté');

            return $this->redirect($this->generateUrl('group_documents', ['id' => $group->getId()]));
        }

        $this->createFormViews($params);
        $params['documents'] = $this->getDocuments($group);

        return $params;
    }
}
