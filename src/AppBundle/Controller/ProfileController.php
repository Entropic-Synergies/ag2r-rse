<?php


namespace AppBundle\Controller;

use AppBundle\Entity\User;
use ES\Bundle\UserBundle\Controller\UtilsTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ProfileController extends Controller
{
    use UtilsTrait;

    /**
     * @Route("/profile/picture/upload", name="profile_picture_upload")
     */
    public function uploadAction()
    {
        /** @var User $user */
        $user        = $this->getUser();
        $fileManager = $this->get('es_fileupload.file_upload_manager');

        return $fileManager->handleForm(function ($picture, $em) use ($user) {
            $user->setPicture($picture);
        });
    }
} 