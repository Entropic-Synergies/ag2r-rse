<?php

namespace AppBundle\Controller;

use ES\Bundle\SocialBundle\Model\SocialObjectInterface;
use ES\Bundle\UserBundle\Controller\UtilsTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    use UtilsTrait;

    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $edito = $this->get('es_cms.content_manager')->getContent('edito', function () {
            return file_get_contents(__DIR__ . '/../Resources/fixtures/cms/default_edito.html');
        });

        /** @var SocialObjectInterface $user */
        $user = $this->getUser(false);
        if (!$user) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $feeds = $this->get('es_social.feed_manager')->getFeeds($user->getSubjectReference(), $this->get('request'));

        if ($request->isXmlHttpRequest()) {
            return $this->render('AppBundle:Feed:feedsContent.html.twig', [
                'feeds' => $feeds,
            ]);
        }

        $feedFormHandler = $this->get('es_social.form.feed');
        $feedForm        = $feedFormHandler->getForm(null, null, [
            'pictures_enabled' => false,
        ]);
        $feedForm->add('submit', 'submit', [
            'label' => 'Publier',
        ]);

        if ($feedFormHandler->handleForm($feedForm, $request)) {
            $this->addFlash('success', 'Votre message a bien été publié');

            return $this->redirect($this->generateUrl('homepage'));
        }

        $ideaForm = $this->createForm('idea_form', null, [
            'action' => $this->generateUrl('idea_create'),
        ]);

        return [
            'edito'    => $edito,
            'feeds'    => $feeds,
            'feedForm' => $feedForm->createView(),
            'ideaForm' => $ideaForm->createView(),
        ];
    }

    /**
     * @Route(path="/edito", name="edito")
     */
    public function editoAction()
    {
        $content = $this->get('es_cms.content_manager')->getContent('edito_full', function () {
            return file_get_contents(__DIR__ . '/../Resources/fixtures/cms/default_edito.html');
        });

        return $this->render('AppBundle:Cms:content.html.twig', [
            'content' => $content,
        ]);
    }

    /**
     * @Route(path="/instructions", name="instructions")
     */
    public function instructionsAction()
    {
        $content = $this->get('es_cms.content_manager')->getContent('instructions', function () {
            return file_get_contents(__DIR__ . '/../Resources/fixtures/cms/instructions.html');
        });

        return $this->render('AppBundle:Cms:content.html.twig', [
            'content' => $content,
        ]);
    }

    /**
     * @Route(path="/accept-terms-modal", name="accept_terms_modal")
     */
    public function acceptTermsModalAction()
    {
        return $this->render('AppBundle:Common:acceptTerms.html.twig');
    }
}
