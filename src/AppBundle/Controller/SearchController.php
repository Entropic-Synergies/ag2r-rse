<?php

namespace AppBundle\Controller;

use Elastica\Query\QueryString;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/search")
 */
class SearchController extends Controller
{
    /**
     * @Route("/", name="search_index")
     * @Template()
     */
    public function searchAction(Request $request)
    {
        $service = 'fos_elastica.finder.search.user';
//        if (($type = $request->get('t')) && $type !== 'all') {
//            $service .= '.' . $type;
//        }
        /** @var PaginatedFinderInterface $finder */
        $finder = $this->container->get($service);

        $params                 = [];
        $queryString            = $request->get('q');
        $params['search_query'] = $queryString;

        $page = $request->get('page', 1);
        if (null !== $queryString) {
            if (strlen($queryString) > 2) {
                $searchQuery = new QueryString($queryString);
                $searchQuery->setDefaultOperator('AND');
                $searchQuery->setParam('fields', [
                    'name',
                    'description',
                    'firstname',
                    'lastname',
                ]);

                /** @var \Pagerfanta\Pagerfanta $paginator */
                $paginator = $finder->findPaginated($searchQuery);
                $paginator->setCurrentPage($page);
                $count = $paginator->getNbResults();

                $params['count']   = $count;
                $params['results'] = $paginator;
            } else {
                $params['error'] = 'Vous devez saisir au moins 3 caractères';
            }
        }

//        $params['types'] = [
//            'all'  => 'Tout',
//            'idea' => 'Idées',
//            'group' => 'Groupes',
//            'user' => 'Membres'
//        ];

        return $params;
    }
}