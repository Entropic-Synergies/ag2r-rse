<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Group;
use AppBundle\Entity\Idea;
use AppBundle\Entity\User;
use AppBundle\Workflow\IdeaWorkflow;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminController extends CRUDController
{
    public function changeStatusAction()
    {
        $request = $this->get('request');
        $id      = $request->get($this->admin->getIdParameter());
        $status  = $request->get('status');
        $message = $request->request->get('message');

        /** @var Idea $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $object->setAdminMessage($message);

        $ideaManager = $this->get('ag2r.idea.manager');
        $ideaManager->reachNextState($object, $status, $this->getUser());

        $this->addFlash('sonata_flash_success', 'Changement d\'état réussi');

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function changeGroupStatusAction()
    {
        $request = $this->get('request');
        $id      = $request->get($this->admin->getIdParameter());
        $status  = $request->get('status');

        /** @var Group $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $object->setStatus($status);
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($object);
        $em->flush();

        /** @var User $user */
        $user                = $this->getUser();
        $notificationManager = $this->container->get('es_notification.notification_manager');
        $notification        = $notificationManager->createNotification(
            $user->getSubjectReference(),
            'group_status_change.' . $object->getStatus(),
            $object->getSubjectReference()
        );
        $notificationManager->notifyUsers($notification, [$object->getOwner()]);

        $this->addFlash('sonata_flash_success', 'Changement d\'état réussi');

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
} 