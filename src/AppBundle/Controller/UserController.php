<?php


namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Type\UserSearchFormType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use ES\Bundle\BaseBundle\Controller\Select2AjaxDoctrineControllerTrait;
use ES\Bundle\UserBundle\Model\GroupRequest;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    use Select2AjaxDoctrineControllerTrait;

    /**
     * @Route("/users/suggest", name="users_suggest")
     * @Template()
     */
    public function suggestAction()
    {
        $suggest = $this->get('es_base.suggest.factory')->createSuggest('friends_of_friends', [
            'auth_user' => $this->getUser(),
        ]);

        return [
            'users' => $suggest->getResult()->get(10),
        ];
    }

    /**
     * @Route("/users/contacts", name="user_contacts")
     * @Template()
     */
    public function contactsAction()
    {
        $friendManager = $this->get('es_user.friend_manager');

        $friends = $friendManager->getFriendsQuery($this->getUser())
            ->getResult();

        $friendRequests = $friendManager->getPendingFriendsRequests($this->getUser());

        return [
            'friends'        => $friends,
            'friendRequests' => $friendRequests,
        ];
    }

    /**
     * @Route("/terms", name="terms")
     * @Template
     */
    public function termsAction(Request $request)
    {
        if ($request->getMethod() === 'POST') {
            if ($request->request->get('termsAccepted')) {
                /** @var User $user */
                $user = $this->getUser();
                $user->setTermsAccepted(true);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Vous avez accepté les conditions générales d\'utilisation');

                return $this->render('AppBundle:User:termsAccepted.html.twig');
            }

            return ['modal' => false];
        }

        return ['modal' => true];
    }

    /**
     * @Route("/users/ajax", name="users_ajax")
     */
    public function select2UsersAction(Request $request)
    {
        return $this->select2DoctrineAjaxAction('AppBundle\Entity\User', function (User $user) {
            return [
                'id'      => $user->getId(),
                'text'    => $user->getFullname(),
                'picture' => $user->getPicture(),
            ];
        }, ['firstname', 'lastname']);
    }

    /**
     * @Route("/users/{id}", name="user_show")
     * @Template()
     */
    public function showAction($id)
    {
        $userClass = $this->container->getParameter('fos_user.model.user.class');

        /** @var ObjectManager $om */
        $om   = $this->get('es_base.object_manager');
        $user = $om->getRepository($userClass)->find($id);

        if (!$user instanceof User) {
            throw $this->createNotFoundException('User not found');
        }

        $params = [
            'user' => $user,
        ];

        if ($authUser = $this->getUser()) {
            $friendRequest           = $this->get('es_user.friend_manager')->findFriendRequest($authUser, $user);
            $params['friendRequest'] = $friendRequest;
        }

        return $params;
    }
} 