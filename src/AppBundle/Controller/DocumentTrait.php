<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use AppBundle\Entity\SocialReference;
use AppBundle\Form\Type\DocumentFormType;
use Doctrine\ORM\EntityManager;
use ES\Bundle\SocialBundle\Model\SocialObjectInterface;
use ES\Bundle\UserBundle\Controller\UtilsTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

trait DocumentTrait
{
    protected function handleForm(Request $request, SocialObjectInterface $object)
    {
        /** @var Controller|UtilsTrait $this */
        $user = $this->getUser();
        /** @var FormInterface $form */
        $form = $this->createForm(new DocumentFormType());

        $form->handleRequest($request);
        if ($form->isValid()) {
            /** @var Document $data */
            $data = $form->getData();

            $data->setUser($user);

            /** @var SocialReference $subject */
            $subject = $object->getSubjectReference();
            $data->setObject($subject);

            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            $this->addFlash('success', 'Le document a bien été ajouté');
        }

        return $form;
    }

    /**
     * @param SocialObjectInterface $object
     * @return Document[]
     */
    protected function getDocuments(SocialObjectInterface $object)
    {
        /** @var Controller|UtilsTrait $this */

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('AppBundle:Document')
            ->findBy([
                'object' => $object->getSubjectReference()->getId(),
            ]);
    }
} 