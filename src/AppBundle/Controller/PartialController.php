<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PartialController extends Controller
{
    public function ideaCategoriesAction()
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:IdeaCategory')->findBy([
            'group' => null,
        ]);

        return $this->render('AppBundle:Idea:sidebarCategories.html.twig', [
            'categories' => $categories,
        ]);
    }

    public function suggestAction()
    {
        $suggest = $this->get('es_base.suggest.factory')->createSuggest('friends_of_friends', [
            'auth_user' => $this->getUser(),
        ]);

        return $this->render('AppBundle:User:suggestWidget.html.twig', [
            'users' => $suggest->getResult()->get(4),
        ]);
    }
}
