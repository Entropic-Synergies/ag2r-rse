<?php

namespace AppBundle\Timeline\Dispatcher;

use AppBundle\Entity\Idea;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use ES\Bundle\SocialBundle\Model\FeedChannel;
use ES\Bundle\SocialBundle\Model\FeedChannelInterface;
use ES\Bundle\SocialBundle\Model\FeedInterface;
use ES\Bundle\SocialBundle\Timeline\Dispatcher\TimelineDispatcherInterface;
use ES\Bundle\SocialBundle\Timeline\Dispatcher\TimelineEntry;

class TimelineIdeaDispatcher implements TimelineDispatcherInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FeedInterface $feed
     * @return boolean
     */
    public function supports(FeedInterface $feed)
    {
        return $feed->getVerb() === 'create' && $feed->getObject()->getObject() instanceof Idea;
    }

    /**
     * @param FeedInterface   $feed
     * @param ArrayCollection $collection
     */
    public function dispatch(FeedInterface $feed, ArrayCollection $collection)
    {
        /** @var Idea $idea */
        $idea = $feed->getObject()->getObject();
        if (null !== $group = $idea->getGroup()) {
            $collection->add(new TimelineEntry($group->getSubjectReference(), false));

            if ($group->isOpen()) {
                $collection->add(new TimelineEntry($idea->getOwner()->getSubjectReference(), false));
            }
        } else {
            $channel = $this->em->getRepository('ES\Bundle\SocialBundle\Model\FeedChannel')->findOneBy(['name' => 'main']);
            if (!$channel instanceof FeedChannelInterface) {
                throw new \InvalidArgumentException('Main channel not found');
            }
            $collection->add(new TimelineEntry($channel->getSubjectReference(), true));
        }
    }
}