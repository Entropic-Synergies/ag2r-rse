<?php

namespace AppBundle\Timeline\Dispatcher;

use AppBundle\Entity\Document;
use AppBundle\Entity\Group;
use Doctrine\Common\Collections\ArrayCollection;
use ES\Bundle\SocialBundle\Model\FeedInterface;
use ES\Bundle\SocialBundle\Timeline\Dispatcher\TimelineDispatcherInterface;
use ES\Bundle\SocialBundle\Timeline\Dispatcher\TimelineEntry;

class TimelineDocumentDispatcher implements TimelineDispatcherInterface
{
    /**
     * @param FeedInterface $feed
     * @return boolean
     */
    public function supports(FeedInterface $feed)
    {
        return $feed->getVerb() === 'add' && $feed->getObject()->getObject() instanceof Document;
    }

    /**
     * @param FeedInterface   $feed
     * @param ArrayCollection $collection
     */
    public function dispatch(FeedInterface $feed, ArrayCollection $collection)
    {
        $group = $feed->getContext()->getObject();
        if ($group instanceof Group) {
            $collection->add(new TimelineEntry($feed->getContext(), false));
        }
    }
}