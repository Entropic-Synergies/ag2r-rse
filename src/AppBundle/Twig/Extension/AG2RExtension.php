<?php
namespace AppBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class AG2RExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFilters()
    {
        return [
            'merge_request_params' => new \Twig_Filter_Function([
                $this,
                'mergeRequestParams'
            ]),

        ];
    }

    public function mergeRequestParams(array $defaultParams, array $params = [])
    {
        if (!$this->container->isScopeActive('request')) {
            throw new \InvalidArgumentException('Scope "request" is required or merge_request_params twig filter');
        }

        $request = $this->container->get('request');

        return array_merge_recursive($defaultParams, $request->query->all(), $params);
    }

    public function getName()
    {
        return 'ag2r';
    }
}