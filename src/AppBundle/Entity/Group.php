<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ES\Bundle\SocialBundle\Model\ReferenceableTrait;
use ES\Bundle\SocialBundle\Model\SocialObjectInterface;
use ES\Bundle\UserBundle\Model\Group as BaseGroup;
use ES\Bundle\UserBundle\Model\RequestStatusInterface;

/**
 * @ORM\Entity
 */
class Group extends BaseGroup implements RequestStatusInterface, SocialObjectInterface
{
    use ReferenceableTrait;

    /**
     * @ORM\Column(type="string", length=15)
     * @var string
     */
    private $status = self::STATUS_PENDING;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $ideasCount = 0;

    static public function getAvailableStatutes()
    {
        return [
            static::STATUS_CONFIRMED,
            static::STATUS_DECLINED,
            static::STATUS_PENDING,
            static::STATUS_REMOVED,
        ];
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        if (!in_array($status, static::getAvailableStatutes())) {
            throw new \InvalidArgumentException(sprintf('Invalid value "%s". Expected one of "%s"',
                    $status,
                    implode('", "', static::getAvailableStatutes()))
            );
        }
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function isConfirmed()
    {
        return $this->status === self::STATUS_CONFIRMED;
    }

    public function countRealMembers()
    {
        return $this->countUsers() - $this->countAdministrators();
    }

    /**
     * @return int
     */
    public function countIdeas()
    {
        return $this->ideasCount;
    }

    /**
     * @param int $inc
     * @return $this
     */
    public function updateIdeasCount($inc)
    {
        $this->ideasCount += $inc;

        return $this;
    }
}