<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="idea_categories")
 * @ORM\Entity()
 */
class IdeaCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Group
     * @ORM\ManyToOne(targetEntity="Group")
     */
    protected $group;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $operator;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup(Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return User
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param User $operator
     */
    public function setOperator(User $operator = null)
    {
        $this->operator = $operator;

        return $this;
    }

    function __toString()
    {
        return $this->getName() ?: '';
    }
}
