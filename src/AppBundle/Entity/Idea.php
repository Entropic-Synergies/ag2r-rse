<?php


namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ES\Bundle\BaseBundle\Entity\Traits\TimestampableTrait;
use ES\Bundle\FileUploadBundle\Model\File;
use ES\Bundle\NotificationBundle\Mapping\Annotation\Notifyable;
use ES\Bundle\SocialBundle\Model\ReferenceableTrait;
use ES\Bundle\SocialBundle\Model\SocialObjectInterface;
use ES\Bundle\SocialBundle\Mapping\Annotation\Referenceable;
use Doctrine\ORM\Mapping as ORM;
use ES\Bundle\BaseBundle\Mapping\Annotation\AclOwner;

/**
 * @ORM\Table(name="ideas", indexes={@ORM\Index(name="status_idx", columns={"status"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IdeaRepository")
 * @Referenceable()
 * @Notifyable(subscriber="owner")
 * @AclOwner(permissions="view|edit|delete")
 */
class Idea implements SocialObjectInterface
{
    use ReferenceableTrait;
    use TimestampableTrait;

    const STATUS_DRAFT = 'draft';
    const STATUS_ADMIN_PENDING = 'admin_pending';
    const STATUS_STUDY_PENDING = 'study_pending';
    const STATUS_STUDY = 'study';
    const STATUS_DUPLICATE = 'duplicate';
    const STATUS_ESTIMATION = 'estimation';
    const STATUS_EXPERIMENTATION = 'experimentation';
    const STATUS_IMPLEMENTED = 'implemented';
    const STATUS_REFUSED = 'refused';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $owner;

    /**
     * @var User[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="idea_operators")
     */
    protected $operators;

    /**
     * @var string
     */
    protected $adminMessage;

    /**
     * @var Group
     * @ORM\ManyToOne(targetEntity="Group")
     */
    protected $group;

    /**
     * @var IdeaCategory
     * @ORM\ManyToOne(targetEntity="IdeaCategory", fetch="EAGER")
     */
    protected $category;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     */
    protected $status = self::STATUS_DRAFT;

    /**
     * @var File
     * @ORM\ManyToOne(targetEntity="ES\Bundle\FileUploadBundle\Model\File", cascade={"persist", "remove"})
     */
    protected $picture;

    function __construct()
    {
        $this->operators = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param File $picture
     * @return $this
     */
    public function setPicture(File $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return File
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    public function isOwnerTermsAccepted()
    {
        return $this->owner && $this->owner->isTermsAccepted();
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return IdeaCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param IdeaCategory $category
     */
    public function setCategory(IdeaCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup(Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    public function countDocuments()
    {
        return $this->subjectReference->countDocuments();
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getOperators()
    {
        return $this->operators;
    }

    /**
     * @param User $operator
     */
    public function addOperator(User $operator)
    {
        if (!$this->operators->contains($operator)) {
            $this->operators->add($operator);
        }

        return $this;
    }

    /**
     * @param User $operator
     */
    public function removeOperator(User $operator)
    {
        $this->operators->removeElement($operator);

        return $this;
    }

    /**
     * @return string
     */
    public function getAdminMessage()
    {
        return $this->adminMessage;
    }

    /**
     * @param string $adminMessage
     */
    public function setAdminMessage($adminMessage)
    {
        $this->adminMessage = $adminMessage;

        return $this;
    }

    public function isVisible()
    {
        return in_array($this->status, [
            self::STATUS_STUDY_PENDING,
            self::STATUS_STUDY,
            self::STATUS_ESTIMATION,
            self::STATUS_IMPLEMENTED,
            self::STATUS_EXPERIMENTATION,
        ], true);
    }

    function __toString()
    {
        return $this->getName() ?: '';
    }
}
