<?php


namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ES\Bundle\SocialBundle\Model\SocialReference as SocialReferenceBase;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="subject_references")
 */
class SocialReference extends SocialReferenceBase
{
    /**
     * @var Document[]
     * @ORM\OneToMany(targetEntity="Document", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $documents;

    function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function addDocument(Document $document)
    {
        $document->setObject($this);
        $this->documents->add($document);

        return $this;
    }

    /**
     * @return Document[]
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    public function countDocuments()
    {
        return count($this->documents);
    }
} 