<?php


namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ES\Bundle\FileUploadBundle\Model\File;
use ES\Bundle\MessageBundle\Entity\MessagesCountTrait;
use ES\Bundle\MessageBundle\Model\UserMessagesInterface;
use ES\Bundle\NotificationBundle\Model\UserNotificationsTrait;
use ES\Bundle\SocialBundle\Model\ReferenceableTrait;
use ES\Bundle\SocialBundle\Model\SocialObjectInterface;
use ES\Bundle\SocialBundle\Mapping\Annotation\Referenceable;
use ES\Bundle\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use FOS\MessageBundle\Model\ParticipantInterface;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity()
 * @Referenceable()
 */
class User extends BaseUser implements SocialObjectInterface, ParticipantInterface, UserMessagesInterface
{
    use UserNotificationsTrait;
    use ReferenceableTrait;
    use MessagesCountTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $firstname;

    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $lastname;

    /**
     * @var string
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    protected $gender;

    /**
     * @var File
     * @ORM\ManyToOne(targetEntity="ES\Bundle\FileUploadBundle\Model\File", cascade={"persist", "remove"},
     *                                                                      fetch="EAGER")
     */
    protected $picture;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $loginName;

    /**
     * @var string
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $termsAccepted;

    /**
     * @var Interest[]
     * @ORM\ManyToMany(targetEntity="Interest")
     */
    protected $interests;

    public function __construct()
    {
        parent::__construct();
        $this->interests = new ArrayCollection();
    }

    /**
     * @param File $picture
     * @return $this
     */
    public function setPicture(File $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return File
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return mixed
     */
    public function getLoginName()
    {
        return $this->loginName;
    }

    /**
     * @param mixed $loginName
     */
    public function setLoginName($loginName)
    {
        $this->loginName = $loginName;

        return $this;
    }

    public function getIntranetProfileUrl()
    {
        return 'http://intranet.ag2rlamondiale.fr/EspaceAnnuaire/Pages/VisuProfil.aspx?LoginName=' . urlencode($this->getLoginName());
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isTermsAccepted()
    {
        return $this->termsAccepted;
    }

    /**
     * @param boolean $termsAccepted
     */
    public function setTermsAccepted($termsAccepted)
    {
        $this->termsAccepted = $termsAccepted;

        return $this;
    }

    public function addInterest(Interest $interest)
    {
        $this->interests->add($interest);
    }

    public function removeInterest(Interest $interest)
    {
        $this->interests->removeElement($interest);
    }

    /**
     * @return Interest[]
     */
    public function getInterests()
    {
        return $this->interests;
    }

    function __toString()
    {
        return $this->getFullname() ?: '';
    }
}
