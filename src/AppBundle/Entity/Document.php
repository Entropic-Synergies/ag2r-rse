<?php


namespace AppBundle\Entity;

use ES\Bundle\BaseBundle\Entity\Traits\TimestampableTrait;
use ES\Bundle\FileUploadBundle\Model\File;
use ES\Bundle\SocialBundle\Model\ReferenceableTrait;
use ES\Bundle\SocialBundle\Model\SocialObjectInterface;
use ES\Bundle\SocialBundle\Mapping\Annotation\Referenceable;
use Doctrine\ORM\Mapping as ORM;
use ES\Bundle\BaseBundle\Mapping\Annotation\AclOwner;
use ES\Bundle\SocialBundle\Model\SocialReference as SR; // Solution for an unknown error thrown while caching

/**
 * @ORM\Table(name="documents")
 * @ORM\Entity()
 * @Referenceable()
 * @AclOwner(owner="user")
 */
class Document implements SocialObjectInterface
{
    use ReferenceableTrait;
    use TimestampableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * @var SR
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SocialReference", fetch="EAGER", inversedBy="documents")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $object;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * For security
     *
     * @var string
     * @ORM\Column(type="string", length=32)
     */
    protected $hash;

    /**
     * @var File
     * @ORM\ManyToOne(targetEntity="ES\Bundle\FileUploadBundle\Model\File", cascade={"persist", "remove"},
     *                                                                      fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $file;

    function __construct()
    {
        $this->hash = md5(uniqid(time()));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile(File $file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return SR
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param SR $object
     */
    public function setObject(SR $object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }
}
