## AG2R RSE

### Web

- **Staging**:
    - https://ag2r-rse.st.entropic-group.com/ + `sobdqiy3g5b`
- **Preprod**:
    - http://ag2r-rse.pr.entropic-group.com/ + `sobdqiy3g5b`

### Roles

- Chef de projet: rfulchiron@entropic-synergies.com
- Lead dev: mcarmona@entropic-synergies.com

### Demo credentials
- admin@entropic-synergies.com / xxx
